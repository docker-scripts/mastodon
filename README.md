# mastodon in a container

https://docs.joinmastodon.org/

## Installation

  - First install `ds` and `revproxy`:
    + https://gitlab.com/docker-scripts/ds#installation
    + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull mastodon`

  - Create a directory for the container: `ds init mastodon @toot.example.org`

  - Fix the settings: `cd /var/ds/toot.example.org/ ; vim settings.sh`

  - Make the container: `ds make`

## Configuration

Edit the file `mastodon/live/.env.production`. For more details see:
https://docs.joinmastodon.org/admin/config/

In particular, make sure to set the SMTP variables, so that
notifications can be sent to users. If you don't have an SMTP server,
you can install one, following [these
instructions](https://gitlab.com/docker-scripts/postfix/-/blob/master/docs/1-send-email-only-from-trusted-hosts.md).

## Usage

- Make a user admin (Owner):

  ```bash
  ds tootctl accounts modify --help
  ds tootctl accounts modify alice --role Owner
  ```

- Create a new user:

  ```bash
  ds tootctl accounts create --help
  ds tootctl accounts create alice \
      --email alice@example.com \
      --confirmed \
      --role Owner
  ```

  A randomly generated password will be shown in the terminal.

- Backup and restore:

  ```bash
  ds backup
  ds restore <backup-file.tgz>
  ```

- Upgrade to the latest version of mastodon:

  ```bash
  ds upgrade
  ```

- Update the docker image (with the latest updates of packages):

  ```bash
  ds make
  ```

- Rebuild the container:

  ```bash
  ds remake
  ```

  This is like: backup, reinstall everything from scratch, and
  restore.

  CAUTION: If you have made any custom modifications, for example to
  the postgresql config files, you should apply them again, because
  `ds remake` will remove the directory `etc/` (along the other
  directories) and create it again from scratch.

  To make sure that you don't forget any custom modifications, you can
  override and extend the command `ds remake` by creating the script
  `cmd/remake.sh` in the container directory, with a content like
  this:

  ```bash
  rename_function cmd_remake orig_cmd_remake
  cmd_remake() {
      orig_cmd_remake
      # custom modifications go here
  }
  ```

## External services

### External Redis

To use an external redis server, edit `settings.sh` and make sure to
uncomment the corresponding settings:

```bash
### Redis
### Uncomment this setting in order to use an external redis.
### Leave it commented for using the internal redis.
#REDIS_URL=redis://[user]:password@redis.example.org:6378/toot.example.org
```

### External DB

To use an external DB, edit `settings.sh` and make sure to uncomment
the corresponding settings:

```bash
### PostgreSQL
### Uncomment this setting in order to use an external database.
### Leave it commented for using the internal postgresql+pgbouncer.
#DATABASE_URL=postgresql://dbuser:dbpass@dbhost:5432/dbname
```

On the remote database make sure to create a user with a password,
like this:

```bash
su - postgres
db_user=mastodon
db_pass=JLEtFxPrreMvz7Xr
createuser --createdb $db_user
psql -c "ALTER USER $db_user WITH PASSWORD '$db_pass';"
```

## Tips

### Use a different web domain

By default, the variable `WEB_DOMAIN` is optional, and it takes the
value of `LOCAL_DOMAIN`, for example `toot.example.org`. But it is also
possible to set them like this:

```bash
LOCAL_DOMAIN=example.org
WEB_DOMAIN=toot.example.org
```

In this case, the address of the user `alice` will be
`@alice@example.org` (instead of `@alice@toot.example.org`), but the
url of the profile will be `https://toot.example.org/@alice`.

As explained on the
[docs](https://docs.joinmastodon.org/admin/config/#web_domain), in
this setup we need to redirect requests to
`https://example.org/.well-known/webfinger` to
`https://toot.example.org/.well-known/webfinger` (on the web server of
the domain `example.org`).

With nginx the additional configuration looks like this:

```
location /.well-known/webfinger {
  return 301 https://toot.example.org$request_uri;
}
```

With apache2 it looks like this:

```
<Location /.well-known/webfinger>
    Redirect permanent https://toot.example.org/.well-known/webfinger
</Location>
```