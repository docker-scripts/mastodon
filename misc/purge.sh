cmd_purge_help() {
    cat <<_EOF
    purge
        Improve Mastodon’s disk usage.

_EOF
}

cmd_purge() {
    # Prune remote accounts that never interacted with a local user.
    ds tootctl accounts prune

    # Remove remote statuses that local users never interacted with
    # older than 4 days.
    ds tootctl statuses remove --days 4

    # Remove media attachments older than 4 days.
    ds tootctl media remove --days 4

    # Remove all headers (including people I follow).
    ds tootctl media remove --remove-headers --include-follows --days 0

    # Remove link previews older than 4 days.
    ds tootctl preview_cards remove --days 4

    # Remove files not linked to any post.
    ds tootctl media remove-orphans
}
