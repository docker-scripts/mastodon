include(bookworm)

RUN <<EOF
  apt update
  apt upgrade --yes
  apt install --yes \
      curl \
      wget \
      gnupg \
      apt-transport-https \
      lsb-release \
      ca-certificates

  # node.js
  gpg_key=/etc/apt/keyrings/nodesource.gpg
  curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key \
      | gpg --dearmor -o $gpg_key
  echo "deb [signed-by=$gpg_key] https://deb.nodesource.com/node_20.x nodistro main" \
      | tee /etc/apt/sources.list.d/nodesource.list

  # postgresql
  gpg_key=/usr/share/keyrings/postgresql.asc
  wget -O $gpg_key https://www.postgresql.org/media/keys/ACCC4CF8.asc
  repo=$(lsb_release -cs)-pgdg
  echo "deb [signed-by=$gpg_key] http://apt.postgresql.org/pub/repos/apt $repo main" \
        > /etc/apt/sources.list.d/postgresql.list

  # system packages
  apt update
  DEBIAN_FRONTEND=noninteractive \
    apt install --yes \
        imagemagick \
        ffmpeg \
        libvips-tools \
        libpq-dev \
        libxml2-dev \
        libxslt1-dev \
        file \
        git \
        g++ \
        libprotobuf-dev \
        protobuf-compiler \
        pkg-config \
        gcc \
        autoconf \
        bison \
        build-essential \
        libssl-dev \
        libyaml-dev \
        libreadline-dev \
        zlib1g-dev \
        libncurses5-dev \
        libffi-dev \
        libgdbm6 \
        libgdbm-dev \
        nginx \
        nodejs \
        redis-server \
        redis-tools \
        postgresql \
        postgresql-contrib \
        pgbouncer \
        libidn11-dev \
        libicu-dev \
        libjemalloc-dev

  # yarn
  corepack enable

  # elasticsearch
  gpg_key=/usr/share/keyrings/elastic.gpg
  curl -fsSL https://artifacts.elastic.co/GPG-KEY-elasticsearch \
       | gpg --dearmor -o $gpg_key
  repo_url=https://artifacts.elastic.co/packages/7.x/apt
  echo "deb [signed-by=$gpg_key] $repo_url stable main" \
       > /etc/apt/sources.list.d/elastic-7.x.list
  apt update
  apt install --yes elasticsearch

  # create a user
  adduser --disabled-password mastodon

  # backup directories that will be mounted
  cp -a /home/mastodon /home/mastodon1
  cp -a /var/lib/postgresql /var/lib/postgresql1
  cp -a /etc/postgresql /etc/postgresql1
  cp -a /etc/pgbouncer /etc/pgbouncer1
EOF
