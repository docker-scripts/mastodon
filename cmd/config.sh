cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    ds inject msmtp.sh
    ds inject logwatch.sh $(hostname)

    [[ -n $DB_HOST ]] && ds postgresql create

    ds inject setup_db.sh
    ds restart
    ds inject setup.sh

    # create the elasticsearch indices
    ds tootctl search deploy

    _copy_cmd_purge
    _create_cron_job
}

_copy_cmd_purge() {
    [[ -f cmd/purge.sh ]] && return

    mkdir -p cmd
    cp $APPDIR/misc/purge.sh cmd/
}

_create_cron_job() {
    local dir=$(basename $(pwd))
    local name=$(echo $dir | tr . -)
    mkdir -p /etc/cron.d

    [[ -f /etc/cron.d/${name}-purge ]] && return

    # create a cron job that cleans up the storage each day
    cat <<EOF > /etc/cron.d/${name}-purge
# clean up the storage of @$dir each day
0 3 * * *  root  bash -l -c "ds @$dir purge &> /dev/null"
EOF
}
