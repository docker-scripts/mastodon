cmd_tootctl() {
    local opts='i'
    test -t 0 && opts+='t'
    docker exec -$opts \
           -u mastodon -w /home/mastodon/live \
           $CONTAINER \
           env RAILS_ENV=production \
           PATH=/home/mastodon/.rbenv/bin:/home/mastodon/.rbenv/shims:/usr/bin \
           bin/tootctl "$@"
}
