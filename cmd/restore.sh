cmd_restore_help() {
    cat <<_EOF
    restore <backup-file.tgz>
        Restore from the given backup file.

_EOF
}

cmd_restore() {
    local file=$1
    [[ ! -f $file ]] && fail "Usage:\n$(cmd_restore_help)"
    local backup=${file%%.tgz}
    backup=$(basename $backup)

    # extract the backup archive
    tar --extract --gunzip --preserve-permissions --file=$file

    # restore redis
    if [[ -f $backup/dump.rdb ]]; then
        cp $backup/dump.rdb redis/
        ds exec chown redis: -R /var/lib/redis/
    fi
    
    # restore user-uploaded files
    [[ -d $backup/public-system/ ]] &&\
        rsync -a $backup/public-system/ mastodon/live/public/system/

    # fix ownership
    ds exec chown mastodon: -R /home/mastodon/

    # restore the content of the database
    local database=${DATABASE_URL:-mastodon_production}
    ds exec cp /host/$backup/mastodon_production.sql /tmp/
    ds pg psql -f /tmp/mastodon_production.sql -d $database -o /dev/null
    ds exec rm /tmp/mastodon_production.sql
    
    # restore .env.production, but preserve the current DB settings and LOCAL_DOMAIN
    source mastodon/live/.env.production
    cp $backup/env.production mastodon/live/.env.production
    sed -i mastodon/live/.env.production \
        -e "/DATABASE_URL/ c DATABASE_URL=$DATABASE_URL" \
        -e "/LOCAL_DOMAIN/ c LOCAL_DOMAIN=$LOCAL_DOMAIN"

    # restore settings
    #cp $backup/settings.sh .

    # clean up
    rm -rf $backup

    # rebuild the home timelines for each user
    ds tootctl feeds build

    # create the elasticsearch indices
    ds tootctl search deploy
}
