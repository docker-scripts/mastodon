cmd_backup_help() {
    cat <<_EOF
    backup
        Make a backup.

_EOF
}

cmd_backup() {
    # create the backup dir
    local backup="backup-$(date +%Y%m%d)"
    rm -rf $backup
    rm -f $backup.tgz
    mkdir $backup

    # backup settings
    cp settings.sh $backup/

    # backup .env.production
    cp mastodon/live/.env.production $backup/env.production

    # backup redis
    [[ -f redis/dump.rdb ]] && cp redis/dump.rdb $backup/
    
    # backup user-uploaded files
    [[ -d mastodon/live/public/system/ ]] &&\
        cp -a mastodon/live/public/system/ $backup/public-system
    
    # dump the content of the database
    local database=${DATABASE_URL:-mastodon_production}
    ds pg pg_dump --clean -d $database > $backup/mastodon_production.sql

    # make the backup archive
    tar --create --gzip --preserve-permissions --file=$backup.tgz $backup/
    rm -rf $backup/
}
