cmd_remake_help() {
    cat <<_EOF
    remake
        Rebuild the container, preserving the existing data.

_EOF
}

cmd_remake() {
    set -x
    ds backup

    ds remove
    rm -rf etc/ mastodon/ postgresql/ redis/
    ds make
    ds restart
    sleep 5

    ds restore "backup-$(date +%Y%m%d).tgz"
}
