cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    mkdir -p mastodon redis
    orig_cmd_create \
        --tmpfs /tmp:exec \
        --mount type=bind,src=$(pwd)/mastodon,dst=/home/mastodon \
        --mount type=bind,src=$(pwd)/redis,dst=/var/lib/redis \
        $(mount_postgresql_dirs) \
        "$@"    # accept additional options
}

mount_postgresql_dirs() {
    [[ -n $DATABASE_URL ]] && return

    mkdir -p postgresql etc/postgresql etc/pgbouncer
    cat <<EOF
        --mount type=bind,src=$(pwd)/postgresql,dst=/var/lib/postgresql
        --mount type=bind,src=$(pwd)/etc/postgresql,dst=/etc/postgresql
        --mount type=bind,src=$(pwd)/etc/pgbouncer,dst=/etc/pgbouncer
EOF
}
