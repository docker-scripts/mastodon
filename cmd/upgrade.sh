cmd_upgrade_help() {
    cat <<_EOF
    upgrade <version>
        Upgrade to the given <version> of mastodon.
        Example:   ds upgrade v4.2.0

_EOF
}

verlte() {
    printf '%s\n' "$1" "$2" | sort -C -V
}

verlt() {
    ! verlte "$2" "$1"
}

### Execute a command in the mastodon directory, inside the container.
_masto_exec() {
    docker exec \
           -u mastodon \
           -w /home/mastodon/live \
           $CONTAINER \
           "$@"
}

cmd_upgrade() {
    # get the new version
    local version=$1
    [[ -n $version ]] || fail "Usage:\n$(cmd_upgrade_help)"
    _masto_exec git fetch --tags -qp
    _masto_exec git tag -l | grep -qs -x $version \
        || fail "The version '$version' does not exist."
    verlt "$MASTODON_VERSION" "$version" \
        || fail "Version '$version' is not greater than $MASTODON_VERSION"

    # update mastodon code
    _masto_exec git stash
    _masto_exec git checkout $version
    _masto_exec git stash pop

    # upgrade the app
    cat > mastodon/live/upgrade.sh <<'EOF'
#!/bin/bash

source .env.production
[[ $DATABASE_URL == *localhost:6432* ]] && export DATABASE_URL=${DATABASE_URL//6432/5432}

source ~/.profile

set -x

# update ruby version
RUBY_VERSION=$(cat .ruby-version)
git -C "$(rbenv root)"/plugins/ruby-build pull
RUBY_CONFIGURE_OPTS=--with-jemalloc rbenv install $RUBY_VERSION
rbenv rehash
rbenv global $RUBY_VERSION
gem install bundler --no-document

# upgrade the app
bundle exec rake db:migrate
bundle install --quiet
yarn install --frozen-lockfile --non-interactive
bundle exec rails assets:precompile
bundle exec rake db:migrate

EOF
    chmod +x mastodon/live/upgrade.sh
    _masto_exec ./upgrade.sh
    rm mastodon/live/upgrade.sh

    # restart services
    ds shell systemctl restart \
       mastodon-web \
       mastodon-sidekiq \
       mastodon-streaming
    
    # rebuild the home timelines for each user
    ds tootctl feeds build

    # update settings.sh
    sed -i settings.sh -e "/MASTODON_VERSION/ c MASTODON_VERSION='$version'"
}
