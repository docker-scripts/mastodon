APP=mastodon
DOMAIN="toot.example.org"

MASTODON_VERSION='v4.3.2'

### Limit the JVM Heap Size for ElasticSearch.
### It is usually recommended to be no more than half the size of the
### VPS where Mastodon is running.
#JVM_HEAP_SIZE=1g

### PostgreSQL
### Uncomment this setting in order to use an external database.
### Leave it commented for using the internal postgresql+pgbouncer.
#DB_HOST=postgresql
#DB_NAME="toot_example_org"
#DB_USER="toot_example_org"
#DB_PASS="12345"
#DATABASE_URL=postgresql://${DB_USER}:${DB_PASS}@${DB_HOST}:5432/${DB_NAME}
