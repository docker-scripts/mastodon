#!/bin/bash

rename_function cmd_remove standard_remove
cmd_remove() {
    standard_remove
    rm -f /etc/cron.d/"$(echo $(basename $(pwd)) | tr . -)"-purge
}

