#!/bin/bash -x

source /host/settings.sh

if [[ -n $DATABASE_URL ]]; then
    # an external database is being used, disable postgresql and pgbouncer
    systemctl stop postgresql pgbouncer
    systemctl disable postgresql pgbouncer
    systemctl mask postgresql pgbouncer
else
    # when we install for the first time, these directories are empty
    [[ -z $(ls /var/lib/postgresql/) ]] &&\
        cp -a /var/lib/postgresql1/* /var/lib/postgresql/
    [[ -z $(ls /etc/postgresql/) ]] &&\
           cp -a /etc/postgresql1/* /etc/postgresql/
    [[ -z $(ls /etc/pgbouncer/) ]] &&\
           cp -a /etc/pgbouncer1/* /etc/pgbouncer/

    # make sure that these directories have correct ownership
    chown postgres: -R /var/lib/postgresql/
    chown postgres: -R /etc/postgresql/
    chown postgres: -R /etc/pgbouncer/

    # restart the services
    systemctl restart postgresql pgbouncer
fi

rm -rf /var/lib/postgresql1/ /etc/postgresql1/ /etc/pgbouncer1/
