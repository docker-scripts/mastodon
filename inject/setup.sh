#!/bin/bash -x

source /host/settings.sh

main() {
    setup_redis
    setup_elasticsearch

    if [[ $(ls -A /home/mastodon) ]]; then
        chown mastodon: -R /home/mastodon
        run_as_mastodon 'cd live/ && bundle install && yarn install --immutable'
        run_as_mastodon 'cd live/ && RAILS_ENV=production bundle exec rails assets:precompile'
    else
        cp -a /home/mastodon1/.[^.]* /home/mastodon/
        chown mastodon: -R /home/mastodon/

        get_mastodon
        install_rbenv_and_ruby
        install_mastodon
        local_db && create_db_user
        setup_mastodon
        local_db && setup_pgbouncer
    fi

    setup_mastodon_services
    setup_nginx
    setup_cron
}

run_as_mastodon() {
    su -l mastodon -c "$*"
}

local_db() {
    # test whether we are using a local DB or an external one
    [[ -z $DATABASE_URL ]]
}

setup_redis() {
    if [[ -z $REDIS_URL ]]; then
        chown redis: -R /var/lib/redis/

        # fix redis service
        mkdir -p /lib/systemd/system/redis-server.service.d
        cat <<EOF > /lib/systemd/system/redis-server.service.d/override.conf
[Service]
PrivateUsers=false
ProtectHostname=false
EOF
        systemctl daemon-reload
        systemctl restart redis
    else
        # we are using an external redis server
        systemctl stop redis-server
        systemctl disable redis-server
        systemctl mask redis-server
    fi
}

setup_elasticsearch() {
    sed -i /etc/elasticsearch/elasticsearch.yml \
        -e '/network.host:/ c network.host: localhost'

    cat <<EOF > /etc/elasticsearch/jvm.options.d/jvm-heap-size.options
-Xms${JVM_HEAP_SIZE:-1g}
-Xmx${JVM_HEAP_SIZE:-1g}
EOF

    # fix some warnings about security, see:
    # https://github.com/mastodon/mastodon/issues/18625#issuecomment-1279833466
    cat <<EOF >> /etc/elasticsearch/elasticsearch.yml
discovery.type: single-node

xpack.security.enabled: true

xpack.security.authc:
  anonymous:
    username: anonymous_user
    roles: superuser
    authz_exception: true
EOF

    systemctl daemon-reload
    systemctl enable elasticsearch
    systemctl start elasticsearch
}

get_mastodon() {
    git clone \
        https://github.com/mastodon/mastodon.git \
        /home/mastodon/live
    cd /home/mastodon/live
    git checkout $MASTODON_VERSION
    chown mastodon: -R .
}

install_rbenv_and_ruby() {
    cat >> /home/mastodon/.profile <<'EOF'
export RAILS_ENV=production
export PATH="$HOME/.rbenv/bin:$PATH"
EOF
    chown mastodon: /home/mastodon/.profile

    ### install rbenv
    git clone \
        https://github.com/rbenv/rbenv.git \
        /home/mastodon/.rbenv
    echo 'eval "$(rbenv init -)"' >> /home/mastodon/.profile

    git clone \
        https://github.com/rbenv/ruby-build.git \
        /home/mastodon/.rbenv/plugins/ruby-build
    chown mastodon: -R /home/mastodon/.rbenv/

    ### install ruby
    run_as_mastodon "cd live/ && RUBY_CONFIGURE_OPTS=--with-jemalloc rbenv install"
}

install_mastodon() {
    cat > /home/mastodon/install.sh <<'EOF'
#!/bin/bash -x

cd ~/live/

bundle config deployment 'true'
bundle config without 'development test'
bundle install -j$(getconf _NPROCESSORS_ONLN)
yarn install <<< Y

EOF
    chmod +x /home/mastodon/install.sh
    run_as_mastodon ./install.sh
    rm /home/mastodon/install.sh
}

create_db_user() {
    local db_user=mastodon
    db_pass=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w16 | head -n1)

    # db_pass is a global variable, so that we can use it on
    # setup_mastodon() and setup_pgbouncer() as well

    # create a DB user with db_pass
    su - postgres -c "createuser --createdb $db_user"
    su - postgres -c "psql <<< \"ALTER USER $db_user WITH PASSWORD '$db_pass';\" "

    # enable md5 password authentication for postgresql
    sed -i /etc/postgresql/15/main/pg_hba.conf \
        -e '/^local *all *all *peer/ s/peer/md5/'
    systemctl restart postgresql
}

rails() {
    run_as_mastodon "cd live/ && RAILS_ENV=production bin/rails $@"
}

setup_mastodon() {
    ### this function does the job of 'RAILS_ENV=production bin/rails mastodon:setup'
    ### see: https://docs.joinmastodon.org/admin/install/#generating-a-configuration

    ### create a configuration file
    cat > /home/mastodon/live/.env.production <<EOF
###
# For more options and details see:
# https://docs.joinmastodon.org/admin/config/
###

LOCAL_DOMAIN=$DOMAIN
#SINGLE_USER_MODE=false

### Secrets
SECRET_KEY_BASE=$(rails secret)
OTP_SECRET=$(rails secret)

### Web Push
$(rails mastodon:webpush:generate_vapid_key)

### Enable Active Record Encryption
$(rails db:encryption:init | tail -3)

### PostgreSQL
DATABASE_URL=${DATABASE_URL:-postgresql://mastodon:${db_pass}@localhost:5432/mastodon_production}
PREPARED_STATEMENTS=false

### Sending mail
SMTP_SERVER=smtp.example.org
SMTP_PORT=25
SMTP_LOGIN=
SMTP_PASSWORD=
SMTP_AUTH_METHOD=plain
SMTP_OPENSSL_VERIFY_MODE=none
SMTP_ENABLE_STARTTLS=auto
SMTP_FROM_ADDRESS='Mastodon <noreply@example.org>'

### Elasticsearch
ES_ENABLED=true
#ES_HOST=localhost
#ES_PORT=9200

### Scaling up
#WEB_CONCURRENCY=2
#MAX_THREADS=5

### Misc
RAILS_LOG_LEVEL=warn
EOF
    chown mastodon: /home/mastodon/live/.env.production

    # create the database schema
    rails db:setup

    # precompile assets
    rails assets:precompile
}

setup_pgbouncer() {
    ### See: https://docs.joinmastodon.org/admin/scaling/#pgbouncer

    # create userlist.txt
    local md5_pass=md5$(echo -n "${db_pass}mastodon" | md5sum | cut -d' ' -f1)
    cat <<EOF > /etc/pgbouncer/userlist.txt
"mastodon" "$md5_pass"
"pgbouncer "$md5_pass"
EOF

    # modify pgbouncer.ini
    sed -i /etc/pgbouncer/pgbouncer.ini \
        -e "/^\[databases\]/ a mastodon_production = host=localhost port=5432 dbname=mastodon_production user=mastodon password=$db_pass" \
        -e '/^;admin_users/ a admin_users = pgbouncer' \
        -e '/^;pool_mode/ a pool_mode = transaction'

    # restart the service
    chown postgres: -R /etc/pgbouncer
    systemctl reload pgbouncer

    # change the database port on the config file .env.production
    sed -i /home/mastodon/live/.env.production \
        -e 's/:5432/:6432/'
}

setup_mastodon_services() {
    cp /home/mastodon/live/dist/mastodon-*.service \
       /etc/systemd/system/

    sed -i /etc/systemd/system/mastodon-web.service \
        -e 's/^PrivateUsers=/#PrivateUsers=/' \
        -e 's/^ProtectHostname=/#ProtectHostname=/'
    sed -i /etc/systemd/system/mastodon-sidekiq.service \
        -e 's/^PrivateUsers=/#PrivateUsers=/' \
        -e 's/^ProtectHostname=/#ProtectHostname=/'
    sed -i /etc/systemd/system/mastodon-streaming.service \
        -e 's/^PrivateUsers=/#PrivateUsers=/' \
        -e 's/^ProtectHostname=/#ProtectHostname=/'

    systemctl daemon-reload
    systemctl enable --now \
              mastodon-web \
              mastodon-sidekiq \
              mastodon-streaming
    systemctl start \
              mastodon-web \
              mastodon-sidekiq \
              mastodon-streaming
}

setup_nginx() {
    cp /home/mastodon/live/dist/nginx.conf \
       /etc/nginx/sites-available/mastodon
    ln -s /etc/nginx/sites-available/mastodon \
       /etc/nginx/sites-enabled/mastodon

    sed -i /etc/nginx/nginx.conf \
        -e 's/^user www-data;/user mastodon;/'
    sed -i /etc/nginx/sites-available/default \
        -e 's/listen \[::]:/#listen [::]:/'
    sed -i /etc/nginx/sites-available/mastodon \
        -e 's/listen \[::]:/#listen [::]:/' \
        -e "s/server_name example.com;/server_name $DOMAIN;/" \
        -e 's%.*ssl_certificate .*%  ssl_certificate     /etc/ssl/certs/ssl-cert-snakeoil.pem;%' \
        -e 's%.*ssl_certificate_key .*%  ssl_certificate_key /etc/ssl/private/ssl-cert-snakeoil.key;%'

    systemctl restart nginx
}

setup_cron() {
    cat > /var/spool/cron/crontabs/mastodon <<EOF
@weekly RAILS_ENV=production /home/mastodon/live/bin/tootctl media remove
@weekly RAILS_ENV=production /home/mastodon/live/bin/tootctl preview_cards remove
EOF
    chmod 600 /var/spool/cron/crontabs/mastodon
    chown mastodon:crontab /var/spool/cron/crontabs/mastodon
}

# start main
main "$@"
